document.body.style = "margin: 0px; background: #0e0e0e; display: flex;"
document.body.innerHTML = `<img id="i" style="margin: 0 auto; align-self: center; max-height: 90vh; cursor: zoom-in;" src="` + document.URL + `">`

let i = document.getElementById("i")
let isZoomed = false

i.onclick = (e) => {
	isZoomed = !isZoomed
	if(isZoomed) {
		i.style = "margin: 0 auto; align-self: center; cursor: zoom-out;"
	} else {
		i.style = "margin: 0 auto; align-self: center; max-height: 90vh; cursor: zoom-in;"
	}
}